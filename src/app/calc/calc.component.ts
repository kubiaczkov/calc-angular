import { Component, OnInit } from '@angular/core';
//import { TextfieldComponent } from 'app/textfield/textfield.component';
import { Injectable } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-calc',
  templateUrl: './calc.component.html',
  styleUrls: ['./calc.component.css']
})

export class CalcComponent implements OnInit {
  current: any;
  arr: any;
  flag: any;
  //constructor(private tfcomponent:TextfieldComponent) {
  //this.current=this.btnscomponent.current;
  constructor() {
    this.current = '';
    this.arr = [];
    this.flag = false;
    //this.calculate(this.current);
  }

  @Output() myNewEvent = new EventEmitter();
  calculate(value) {
    //alert(value);
    //alert("inside calc comp "+value);
    if (value == 'C') {
      this.current = '0';
      this.arr = [];
      this.flag = false;
    } else if (value == 'CE') {
      this.current = '0';
    } else if (value == 'avg') {
      this.arr.push(this.current);
      console.log(this.arr);
      this.current = '0';
      this.flag = true;
      // console.log(this.flag);
    } else if (value == 'bcsp') {
      if (this.current.length == 1) {
        this.current = '0';
      } else {
        this.current = this.current.substring(0, this.current.length - 1);
      }
    } else if (value == 'negate') {
      this.current = this.current * -1;
    } else if (value == 'factorial') {
      this.current = this.rFact(this.current);
    } else if (value == 'pi') {
      this.current += Math.PI;
    } else if (value == 'root') {
      this.current = Math.sqrt(this.current);
    } else if (value == 'x2') {
      this.current = Math.pow(this.current, 2);
    } else if (value == 'sin') {
      this.current = Math.sin(this.current);
    } else if (value == 'cos') {
      this.current = Math.cos(this.current);
    } else if (value == 'tan') {
      this.current = Math.tan(this.current);
    } else if (value == 'exp') {
      this.current = Math.exp(this.current);
    } else if (value == 'log') {
      this.current = Math.log(this.current);
    } else if (value == '10x') {
      this.current = Math.pow(10, this.current);
    }
    else if (value == '=') {
      if(this.flag) {
        var suma=0.0,srednia=0.0;
        for(let i=0; i<this.arr.length; i++) {
          suma += parseFloat(this.arr[i]);
        }
        srednia = suma/this.arr.length;
        console.log(srednia);
        this.current = srednia;
      } else {
        this.current = eval(this.current);
      }
      
    }
    else {
      if (this.current == '0') {
        this.current = value;
      }
      else {
        this.current = this.current + value;
      }
    }
    //alert("current "+this.current);
    this.myNewEvent.emit(this.current);
    //this.tfcomponent.displayResult(this.current);
  }
  ngOnInit() {
  }

  rFact(num)
  {
      if (num === 0)
        { return 1; }
      else
        { return num * this.rFact( num - 1 ); }
  }

}
